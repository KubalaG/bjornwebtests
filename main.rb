require 'rubygems'
require 'watir'
require 'uri'
require 'json'
require 'webdrivers/geckodriver'
require 'open3'

require_relative 'functions'
require_relative 'tests'

whatBrowser = ARGV[0]

## Set up a new logger ({homeDirectory}/rubyLogs.log)
logger = Logger.new(Dir.home + '/testWordpress.log')
logger.info('--- Testing Bjorn on localhost ---')

if whatBrowser == 'chrome'
  browser = createCustomChromeBrowser(headless: false, logger: logger)
elsif whatBrowser == 'firefox'
  browser = createCustomBrowser(headless: false, logger: logger)
end
browser = bjornLogIn(browser: browser, logger: logger)

table = CSV.read(Dir.home + '/bjornBits.csv', headers: true)
tunnelDetails = table.select { |row| row['type'] == 'toDelete' }

browser = testCommissions(browser: browser, logger: logger, tunnelDetails: tunnelDetails)
browser = testFinancialReports(browser: browser, logger: logger)
browser = testDocumentation(browser: browser, logger: logger)
browser = testDawnChorus(browser: browser, logger: logger)
browser = testSupplierPOreport(browser: browser, logger: logger)
browser = testBankStatements(browser: browser, logger: logger, tunnelDetails: tunnelDetails)
#browser = testFXTransactions(browser: browser, logger: logger) ## TBD NOT NEEDED IN THE FUTURE??

testRemainingAPIs(logger: logger, tunnelDetails: tunnelDetails)

browser.close