require 'uri'
require 'rubygems'
require 'csv'
require 'date'

## This part is to set browser's download directory to custom one
def createCustomBrowser(args = {})
  args[:logger].info('Creating the browser..')

  ## Show only errors in console
  Selenium::WebDriver.logger.level = :error

  ## Setting up custom download direcotry <-- Not workable with Chrome, hence use of Firefox
  directory = File.expand_path(Dir.home + '/stmnts', File.dirname(__FILE__))
  download_directory = directory
  download_directory.tr!('/', '\\') if Selenium::WebDriver::Platform.windows?

  ## New Firefox Profile with custom download directory ({home}/stmnts).
  ## Browser does not ask whether we want to download certain file types
  profile = Selenium::WebDriver::Firefox::Profile.new
  profile['browser.download.folderList'] = 2 # custom location
  profile['browser.download.dir'] = directory
  profile['browser.download.forbid_open_with'] = true
  profile['browser.helperApps.neverAsk.saveToDisk'] = 'text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream'
  if args[:bankLine] == 'yes'
    profile['browser.download.manager.showWhenStarting'] = false
    profile['browser.download.manager.alertOnEXEOpen'] = false
    profile['browser.helperApps.neverAsk.saveToDisk'] = 'application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream'
    profile['browser.helperApps.alwaysAsk.force'] = false
    profile['browser.download.manager.useWindow'] = false
    profile['services.sync.prefs.sync.browser.download.manager.showWhenStarting'] = false
    profile['pdfjs.disabled'] = true
  end

  ## Create a new headless/not headless Firefox browser with settings above.
  browser = Watir::Browser.new :firefox, profile: profile, headless: args[:headless]
  browser
end

## Chrome not used at this moment
def createCustomChromeBrowser(args = {})
  args[:logger].info('Creating the browser..')

  ## Show only errors in console
  Selenium::WebDriver.logger.level = :error

  ## Setting up custom download direcotry <-- Not workable with Chrome, hence use of Firefox
  directory = File.expand_path(Dir.home + '/stmnts', File.dirname(__FILE__))
  download_directory = directory
  download_directory.tr!('/', '\\') if Selenium::WebDriver::Platform.windows?

  prefs = {
      'download' => {
          'default_directory' => download_directory,
          'prompt_for_download' => false,
      },
      'profile' => {
          'default_content_setting_values' => {'automatic_downloads' => 1}, #for chrome newe 46
      }
  }

  caps = Selenium::WebDriver::Remote::Capabilities.chrome
  caps['chromeOptions'] = {:prefs => prefs}

  browser = Watir::Browser.new :chrome, :desired_capabilities => caps

  browser
end

## Sign in to Bjorn
def bjornLogIn(args = {})
  args[:logger].info('Logging in to Bjorn')
  args[:browser].goto('http://localhost/bjorn/wp-login.php')
  args[:browser].text_field(id: 'user_login').set 'Greg'
  args[:browser].text_field(id: 'user_pass').set 'thetravelnetworkgroup'
  args[:browser].button(id: 'wp-submit').click
  args[:browser]
end

def checkDownloadedFile(args = {})
  Dir.chdir(Dir.home + '/stmnts') do
    file = Dir.glob('*').max_by {|f| File.mtime(f)}

    csv = CSV.read(file)

    if file.to_s.include? args[:fileName] and (csv[0][0] == 'booking reference' or
        csv[0][0] == 'Type' or csv[0][0] == 'booking ref' or csv[0][0] == 'trade code' or
        csv[0][0] == 'tradeCode' or csv[0][0] == 'ABTA/TTA ref')
      args[:logger].info('TEST - ' + args[:logInfo] + ' - SUCCESS')
      puts 'TEST - ' + args[:logInfo] + ' - SUCCESS'
    else
      args[:logger].error('TEST - ' + args[:logInfo] + ' - FAILED')
      puts 'TEST - ' + args[:logInfo] + ' - FAILED'
    end

    File.delete(file)
  end
end