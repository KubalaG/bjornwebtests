require 'uri'
require 'rubygems'
require 'csv'
require 'date'
require 'net/ssh'

def testCommissions(args = {})
  args[:browser].goto('http://localhost/bjorn/?page_id=350')
  sleep(3)

  ##testCommissionReport(browser: args[:browser], logger: args[:logger], tunnelDetails: args[:tunnelDetails])
  #testSummaryReport(browser: args[:browser], logger: args[:logger])
  testAgencyReport(browser: args[:browser], logger: args[:logger])
end

def testCommissionReport(args = {})
  begin
    args[:browser].select_list(id: 'commissionPage').select('Commission Report')
    args[:browser].select_list(id: 'commCompany').select('TTNG')
    args[:browser].select_list(id: 'commBranch', index: 2).option(index: 2).select
    args[:browser].date_field(id: 'startDate').set('01/02/2020')
    args[:browser].date_field(id: 'endDate').set('01/03/2020')
    args[:browser].date_field(id: 'deptDate').set('01/03/2020')
    args[:browser].select_list(id: 'updateDb').select('No')
    args[:browser].select_list(id: 'allBooking').select('Yes')
    args[:browser].button(id: 'callPaymentTable').click
    sleep(2)
    args[:browser].windows.last.use do
      commissionTable = args[:browser].table(index: 0)
      if(commissionTable.rows.count) == 8
        args[:logger].info('TEST - Print Commission Report Table - SUCCESS')
        puts 'TEST - Print Commission Report Table - SUCCESS'
      else
        args[:logger].error('TEST - Print Commission Report Table - FAILED')
        puts 'TEST - Print Commission Report Table - FAILED'
        puts 'Expected: 6 | Actual: ' + commissionTable.rows.count.to_s
      end
      Net::SSH.start("192.168.227.21", args[:tunnelDetails][0][3].to_s.reverse, password: args[:tunnelDetails][0][4].to_s.reverse) do |ssh|
        updateBooking = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/updateBooking?company=ttng&operation=retail&bookingRef=NWG-09494'")
        sleep(2)
        if updateBooking == '1'
          args[:logger].info('TEST - Update Booking (Commission Report) - SUCCESS')
          puts 'TEST - Update Booking (Commission Report) - SUCCESS'
        else
          args[:logger].error('TEST - Update Booking (Commission Report) - FAILED')
          puts 'TEST - Update Booking (Commission Report) - FAILED'
        end
          setCommPaid = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/setCommPaidValues?company=ttng&operation=retail&tradeCode=Q0655&bookingRef=NWG-09494&grossPaid=0&netPaid=0&discountPaid=0&VATpaid=0&commPaid=0'")
        sleep(2)
        if setCommPaid == '{"result":1}'
          args[:logger].info('TEST - Set Comm Paid (Commission Report) - SUCCESS')
          puts 'TEST - Set Comm Paid (Commission Report) - SUCCESS'
        else
          args[:logger].error('TEST - Set Comm Paid (Commission Report) - FAILED')
          puts 'TEST - Set Comm Paid (Commission Report) - FAILED'
        end
      end
    end
    args[:browser].windows.last.close
    args[:browser]
  rescue => error
    args[:logger].error('Commission report error')
    args[:logger].error(error.message.to_s)
    puts error.message.to_s
  end
end

def testSummaryReport(args = {})
  begin
    args[:browser].select_list(id: 'commissionPage').select('Summary Report')
    args[:browser].select_list(id: 'summCompany').select('TTNG')
    args[:browser].execute_script("document.getElementsByClassName('calcMonth')[1].value = '2021-03';")
    args[:browser].button(value: 'Get Report', index: 1).click
    sleep(5)
    args[:browser].windows.last.use do
      if args[:browser].elements(tag_name: 'tr').count > 3
        args[:logger].info('TEST - Print Summary Commission Report - SUCCESS')
        puts 'TEST - Print Summary Commission Report - SUCCESS'
      else
        args[:logger].error('TEST - Print Summary Commission Report - FAILED')
        puts 'TEST - Print Summary Commission Report - FAILED'
        puts 'Expected: 3 | Value: ' + args[:browser].elements(tag_name: 'tr').count.to_s
      end

      args[:browser].button(index: 0).click
      sleep(5)
      args[:browser].windows.last.use do
        if args[:browser].elements(tag_name: 'tr').count > 1
          args[:logger].info('TEST - Print Summary Commission Breakdown - SUCCESS')
          puts 'TEST - Print Summary Commission Breakdown - SUCCESS'
        else
          args[:logger].error('TEST - Print Summary Commission Breakdown - FAILED')
          puts 'TEST - Print Summary Commission Breakdown - FAILED'
          puts 'Expected: 3 | Value: ' + args[:browser].elements(tag_name: 'tr').count.to_s
        end
        args[:browser].windows.last.close
      end
    end
    args[:browser].windows.last.close
    args[:browser]
  rescue => error
    args[:logger].error('Commission report error')
    args[:logger].error(error.message.to_s)
    puts error.message.to_s
  end
end

def testAgencyReport(args = {})
  begin
    args[:browser].select_list(id: 'commissionPage').select('Agency Report')
    args[:browser].select_list(id: 'calcCompany').select('TTNG')
    args[:browser].execute_script("document.getElementsByClassName('calcMonth')[0].value = '2021-03';")
    args[:browser].select_list(name: 'branchTtng').option(index: 4).select
    args[:browser].button(value: 'Get Report', index: 0).click
    sleep(5)
    args[:browser].windows.last.use do
      if args[:browser].div(class: 'paidElementsReport').exists?
        args[:logger].info('TEST - Print Agency Report - SUCCESS')
        puts 'TEST - Print Agency Report - SUCCESS'
      else
        args[:logger].error('TEST - Print Agency Report - FAILED')
        puts 'TEST - Print Agency Report - FAILED'
      end
    end
    args[:browser].windows.last.close
    args[:browser]
  rescue => error
    args[:logger].error('Commission report error')
    args[:logger].error(error.message.to_s)
    puts error.message.to_s
  end
end

def testBankStatements(args = {})
  args[:browser].goto('http://localhost/bjorn/?page_id=394')
  sleep(3)
  begin
    args[:browser].select_list(id: 'bankRecsSelect').select('Banking Reconciliation')
    args[:browser].select_list(id: 'commCompany').select('TTNG')
    args[:browser].select_list(id: 'commOperation').select('Retail')
    args[:browser].select_list(id: 'commBranch').option(index: 3).select
    args[:browser].date_field(id: 'bankStart').set('01/08/2020')
    args[:browser].date_field(id: 'bankEnd').set('30/08/2020')
    args[:browser].button(id: 'produceBankStmnt').click
    sleep(30)
    args[:browser].windows.last.use do
      if args[:browser].elements(tag_name: 'table').count == 57
        args[:logger].info('TEST - Print Bank Statement Report - SUCCESS')
        puts 'TEST - Print Bank Statement Report - SUCCESS'
      else
        args[:logger].error('TEST - Print Bank Statement Report - FAILED')
        puts 'TEST - Print Bank Statement Report - FAILED'
        puts 'Expected: 57 | Value: ' + args[:browser].elements(tag_name: 'table').count.to_s
      end
      Net::SSH.start("192.168.227.21", args[:tunnelDetails][0][3].to_s.reverse, password: args[:tunnelDetails][0][4].to_s.reverse) do |ssh|
        updateBooking = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/updateBankStmntRow?company=ttng&operation=retail&tradeCode=Q0655&id=253&credit=0&debit=-30.01'")
        sleep(2)
        if updateBooking == '1'
          args[:logger].info('TEST - Update Bank Recs (Debit/Credit) - SUCCESS')
          puts 'TEST - Update Bank Recs (Debit/Credit) - SUCCESS'
        else
          args[:logger].error('TEST - Update Bank Recs (Debit/Credit) - FAILED')
          puts 'TEST - Update Bank Recs (Debit/Credit) - FAILED'
          puts 'Output Value: ' + updateBooking.to_s
        end
        setCommPaid = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/updateBankStmntRow?company=ttng&operation=retail&tradeCode=Q0655&id=107&category=suppRefund&comment=yesss'")
        sleep(2)
        if setCommPaid == '1'
          args[:logger].info('TEST - Update Bank Recs (Comments etc) - SUCCESS')
          puts 'TEST - Update Bank Recs (Comments etc) - SUCCESS'
        else
          args[:logger].error('TEST - Update Bank Recs (Comments etc) - FAILED')
          puts 'TEST - Update Bank Recs (Comments etc) - FAILED'
          puts 'Output Value: ' + setCommPaid.to_s
        end
      end
    end
    args[:browser].windows.last.close
    args[:browser]
  rescue => error
    args[:logger].error('Bank Statement Report')
    args[:logger].error(error.message.to_s)
  end
end

def testFXTransactions(args = {})
  args[:browser].goto('http://localhost/bjorn/?page_id=408')
  sleep(3)
  begin
    args[:browser].select_list(id: 'fxSelectedType').select('Add Transaction')
    args[:browser].select_list(id: 'commBranch', index: 1).select('Q0655 Worldchoice Travel Ltd Shrewsbury')
    args[:browser].date_field(id: 'fxDate').set('01/01/2019')
    args[:browser].text_field(id: 'fxValue').set('99.99')
    args[:browser].button(name: 'fxSubmit', index: 1).click
    sleep(2)
    args[:browser].windows.last.use do
      if(args[:browser].element(tag_name: 'h6').text) == 'Thank you for submitting your payment.'
        args[:logger].info('TEST - Add FX transaction - SUCCESS')
      else
        args[:logger].error('TEST - Add FX transaction - FAILED')
      end
    end
    args[:browser].windows.last.close
    args[:browser].select_list(id: 'fxSelectedType').select('Show Transactions')
    args[:browser].select_list(id: 'commBranch', index: 0).select('Q0655 Worldchoice Travel Ltd Shrewsbury')
    args[:browser].date_field(id: 'fxFrom').set('01/09/2020')
    args[:browser].date_field(id: 'fxDateTo').set('07/09/2020')
    args[:browser].button(name: 'fxSubmit', index: 0).click
    args[:browser].windows.last.use do
      fxTable = args[:browser].table(index: 0)
      if fxTable.rows.count == 3
        args[:logger].info('TEST - Show FX Transactions - SUCCESS')
      else
        args[:logger].error('TEST - Show FX Transactions - FAILED')
      end
    end
    args[:browser].windows.last.close
  Net::SSH.start("192.168.224.12", "SimonH", password: "Gr0wth04") do |ssh|
    updateBooking = ssh.exec!("curl -X PUT 'http://127.0.0.1:5000/updateFXdata?company=ttng&operation=test&tradeCode=Q0655&id=329&credit=100.01'")
    sleep(2)
    if updateBooking == '1'
      args[:logger].info('TEST - Update FX Transaction - SUCCESS')
    else
      args[:logger].error('TEST - Update FX Transaction - FAILED')
    end
  end
    args[:browser]
  rescue => error
    args[:logger].error('Bank Statement Report')
    args[:logger].error(error.message.to_s)
  end
end

def testFinancialReports(args = {})
  args[:browser].goto('http://localhost/bjorn/?page_id=291')
  sleep(3)
  testBankingReport(browser: args[:browser], logger: args[:logger])
  testCAAreport(browser: args[:browser], logger: args[:logger])
  testSageExport(browser: args[:browser], logger: args[:logger])

    ## TBD <-- NonACASSupplier seems to be not working
    ##testNonACASsupplierCreditor(browser: args[:browser], logger: args[:logger])

  testSalesReport(browser: args[:browser], logger: args[:logger])
  testSupplementReport(browser: args[:browser], logger: args[:logger])
  testSupplierCreditor(browser: args[:browser], logger: args[:logger])
  testUpdatedBookings(browser: args[:browser], logger: args[:logger])
  args[:browser]
end

def testDocumentation(args = {})
  begin
  args[:browser].goto('http://localhost/bjorn/?page_id=18')
  sleep(3)
  args[:browser].select_list(id: 'documentCompany').select('TTNG')
  args[:browser].select_list(id: 'documentOperation').select('Retail')
  args[:browser].select_list(name: 'branchTtng').option(index: 2).select
  args[:browser].text_field(name: 'bookingRef').set 'NWG-03903'
  args[:browser].button(name: 'callDocument').click
  sleep(5)
  args[:browser].windows.last.use do
    if args[:browser].div(id: 'content').p(index: 0).text == 'Dear Mr Neil Spence,'
      args[:logger].info('TEST - Documentation Balance Letter - SUCCESS')
      puts 'TEST - Documentation Balance Letter - SUCCESS'
    else
      args[:logger].error('TEST - Documentation Balance Letter - FAILED')
      puts 'TEST - Documentation Balance Letter - FAILED'
    end
  end
  args[:browser].windows.last.close
  args[:browser]
  rescue => error
    args[:logger].error('Documentation error')
    args[:logger].error(error.message.to_s)
  end
end

def testDawnChorus(args = {})
  begin
    args[:browser].goto('http://localhost/bjorn/?page_id=16')
    sleep(3)
    args[:browser].select_list(id: 'documentCompany').select('TTNG')
    args[:browser].select_list(id: 'documentOperation').select('Retail')
    args[:browser].date_field(id: 'date').set('23/08/2020')
    args[:browser].button(id: 'callDawnTable').click
    sleep(10)
    args[:browser].windows.last.use do
      dawnChorusTable = args[:browser].table(index: 0)
      if dawnChorusTable.rows.count == 73
        args[:logger].info('TEST - Print Commission Report Table - SUCCESS')
        puts 'TEST - Print Commission Report Table - SUCCESS'
      else
        args[:logger].error('TEST - Print Commission Report Table - FAILED')
        puts 'TEST - Print Commission Report Table - FAILED'
        puts 'Expected: 73 | Actual: ' + dawnChorusTable.rows.count.to_s
      end
    end
    args[:browser].windows.last.close
    args[:browser]
  rescue => error
    args[:logger].error('Dawn Chorus error')
    args[:logger].error(error.message.to_s)
  end
end

def testSupplierPOreport(args = {})
  begin
    args[:browser].goto('http://localhost/bjorn/?page_id=373')
    sleep(3)
    args[:browser].select_list(id: 'getPO').select('Matched Supplier Payments')
    args[:browser].select_list(id: 'commCompany').select('TTNG')
    args[:browser].select_list(id: 'commOperation').select('Retail')
    args[:browser].select_list(id: 'commBranch').option(index: 6).select
    args[:browser].date_field(id: 'collectDateMatch').set('07/08/2020')
    args[:browser].button(id: 'callMatchedPayments').click
    sleep(5)
    args[:browser].windows.last.use do
      if args[:browser].tables.count == 4
        args[:logger].info('TEST - Supplier Payment Out Report - SUCCESS')
        puts 'TEST - Supplier Payment Out Report - SUCCESS'
      else
        args[:logger].error('TEST - Supplier Payment Out Report - FAILED')
        puts 'TEST - Supplier Payment Out Report - FAILED'
        puts 'Expected: | Value: ' + args[:browser].tables.count.to_s
      end
    end
    args[:browser].windows.last.close
    args[:browser]
  rescue => error
    args[:logger].error('Supplier Payments Out error')
    args[:logger].error(error.message.to_s)
  end
end

def testUpdatedBookings(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('Updated Bookings')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].select_list(id: 'branch').option(index: 6).select
    args[:browser].select_list(name: 'operation').select('Retail')
    args[:browser].select_list(name: 'dateTypeUpdatedBookings').select('Booking Date')
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].text_field(name: 'offset').set '10'
    args[:browser].button(id: 'getReport').click
    sleep(15)
    checkDownloadedFile(logger: args[:logger], fileName: 'bookingODRQ0674-bookingDate-10.csv',
                        logInfo: 'Updated Bookings Booking Date')
    args[:browser].select_list(name: 'dateTypeUpdatedBookings').select('Departure Date')
    args[:browser].button(id: 'getReport').click
    sleep(15)
    checkDownloadedFile(logger: args[:logger], fileName: 'bookingODRQ0674-deptDate-10.csv',
                        logInfo: 'Updated Bookings Departure Date')
  rescue => error
    args[:logger].error('Financial report - updated bookings error')
    args[:logger].error(error.message.to_s)
  end
end

def testTAPACASreport(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('TAPACAS Report')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].select_list(id: 'branch').option(index: 5).select
    args[:browser].select_list(name: 'operation').select('Retail')
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].select_list(name: 'dateType').select('Booking Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'TAPACASreport2020-02-01to2020-02-03.csv',
                        logInfo: 'TAPACAS Report Booking Date')
    args[:browser].select_list(name: 'dateType').select('Departure Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'TAPACASreport2020-02-01to2020-02-03.csv',
                        logInfo: 'TAPACAS Report Departure Date')
    args[:browser].select_list(name: 'dateType').select('Transaction Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'TAPACASreport2020-02-01to2020-02-03.csv',
                        logInfo: 'TAPACAS Report Transaction Date')
    args[:browser].select_list(name: 'operation').select('Test')
  rescue => error
    args[:logger].error('Financial report - TAPACAS error')
    args[:logger].error(error.message.to_s)
  end
end

def testSupplierCreditor(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('Supplier Creditor')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].select_list(id: 'branch').option(index: 5).select
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].select_list(name: 'dateType').select('Booking Date')
    args[:browser].button(id: 'getReport').click
    sleep(10)
    checkDownloadedFile(logger: args[:logger], fileName: 'supplierCreditor2020-02-01to2020-02-02.csv',
                        logInfo: 'Supplier Creditor Booking Date')
    args[:browser].select_list(name: 'dateType').select('Departure Date')
    args[:browser].button(id: 'getReport').click
    sleep(7)
    checkDownloadedFile(logger: args[:logger], fileName: 'supplierCreditor2020-02-01to2020-02-02.csv',
                        logInfo: 'Supplier Creditor Departure Date')
    args[:browser].select_list(name: 'dateType').select('Transaction Date')
    args[:browser].button(id: 'getReport').click
    sleep(7)
    checkDownloadedFile(logger: args[:logger], fileName: 'supplierCreditor2020-02-01to2020-02-02.csv',
                        logInfo: 'Supplier Creditor Transaction Date')
  rescue => error
    args[:logger].error('Financial report - supplier creditor error')
    args[:logger].error(error.message.to_s)
  end
end

def testSupplementReport(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('Supplement Report')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].select_list(name: 'operation').select('Retail')
    args[:browser].select_list(id: 'branch').option(index: 5).select
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'supplementsQ0655-bookingDate-2020-02-01-2020-02-02.csv',
                        logInfo: 'Supplement Report')
    args[:browser].select_list(name: 'operation').select('Test')
  rescue => error
    args[:logger].error('Financial report - supplement report error')
    args[:logger].error(error.message.to_s)
  end
end

def testSalesReport(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('Sales Report')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].select_list(id: 'branch').option(index: 5).select
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].select_list(name: 'version').select('Summary')
    args[:browser].select_list(name: 'salesDateType').select('Booking Date')
    args[:browser].button(id: 'getReport').click
    sleep(10)

    checkDownloadedFile(logger: args[:logger], fileName: 'salesReport-Q0655-2020-02-01TO2020-02-02on',
                        logInfo: 'Sales Report Summary Booking Date')
    args[:browser].select_list(name: 'salesDateType').select('Departure Date')
    args[:browser].button(id: 'getReport').click
    sleep(10)
    checkDownloadedFile(logger: args[:logger], fileName: 'salesReport-Q0655-2020-02-01TO2020-02-02on',
                        logInfo: 'Sales Report Summary Departure Date')
    args[:browser].select_list(name: 'salesDateType').select('Transaction Date')
    args[:browser].button(id: 'getReport').click
    sleep(10)
    checkDownloadedFile(logger: args[:logger], fileName: 'salesReport-Q0655-2020-02-01TO2020-02-02on',
                        logInfo: 'Sales Report Summary Transaction Date')
    args[:browser].select_list(name: 'version').select('Full')
    args[:browser].button(id: 'getReport').click
    sleep(10)
    checkDownloadedFile(logger: args[:logger], fileName: 'salesReport-Q0655-2020-02-01TO2020-02-02on',
                        logInfo: 'Sales Report Full Transaction Date')
  rescue => error
    args[:logger].error('Financial report - sales report error')
    args[:logger].error(error.message.to_s)
  end
end

def testNonACASsupplierCreditor(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('Non-ACAS Supplier Creditor')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].select_list(id: 'branch').option(index: 5).select
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].select_list(name: 'dateType').select('Booking Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                        logInfo: 'Banking Report Summary Booking Date')
    args[:browser].select_list(name: 'bankingDateType').select('Departure Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                        logInfo: 'Banking Report Summary Departure Date')
    args[:browser].select_list(name: 'bankingDateType').select('Receipt Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                        logInfo: 'Banking Report Summary Receipt Date')
    args[:browser].select_list(name: 'bankingDateType').select('Transaction Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                        logInfo: 'Banking Report Summary Transaction Date')
    args[:browser].select_list(name: 'version').select('Full')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                        logInfo: 'Banking Report Full Transaction Date')
  rescue => error
    args[:logger].error('Financial report - non-ACAS supplier error')
    args[:logger].error(error.message.to_s)
  end
end

def testBankingReport(args = {})
  begin
  args[:browser].select_list(id: 'whatReport').select('Banking Report')
  args[:browser].select_list(id: 'reportCompany').select('TTNG')
  args[:browser].select_list(id: 'branch').option(index: 5).select
  args[:browser].date_field(id: 'start').set('01/02/2020')
  args[:browser].date_field(id: 'end').set('02/02/2020')
  args[:browser].select_list(name: 'bankingDateType').select('Booking Date')
  args[:browser].button(id: 'getReport').click
  sleep(5)
  checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                      logInfo: 'Banking Report Summary Booking Date')
  args[:browser].select_list(name: 'bankingDateType').select('Departure Date')
  args[:browser].button(id: 'getReport').click
  sleep(5)
  checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                      logInfo: 'Banking Report Summary Departure Date')
  args[:browser].select_list(name: 'bankingDateType').select('Receipt Date')
  args[:browser].button(id: 'getReport').click
  sleep(5)
  checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                      logInfo: 'Banking Report Summary Receipt Date')
  args[:browser].select_list(name: 'bankingDateType').select('Transaction Date')
  args[:browser].button(id: 'getReport').click
  sleep(5)
  checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                      logInfo: 'Banking Report Summary Transaction Date')
  args[:browser].select_list(name: 'version').select('Full')
  args[:browser].button(id: 'getReport').click
  sleep(5)
  checkDownloadedFile(logger: args[:logger], fileName: 'bankingReport2020-02-01to2020-02-02.csv',
                      logInfo: 'Banking Report Full Transaction Date')
  rescue => error
    args[:logger].error('Financial report - banking report error')
    args[:logger].error(error.message.to_s)
  end
end

def testCAAreport(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('CAA Report')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].select_list(id: 'branch').option(index: 5).select
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].select_list(name: 'dateType').select('Booking Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'CAAreport2020-02-01to2020-02-02.csv',
                        logInfo: 'CAA Report Booking Date')
    args[:browser].select_list(name: 'dateType').select('Departure Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'CAAreport2020-02-01to2020-02-02.csv',
                        logInfo: 'CAA Report Departure Date')
    args[:browser].select_list(name: 'dateType').select('Transaction Date')
    args[:browser].button(id: 'getReport').click
    sleep(5)
    checkDownloadedFile(logger: args[:logger], fileName: 'CAAreport2020-02-01to2020-02-02.csv',
                        logInfo: 'CAA Report Transaction Date')
  rescue => error
    args[:logger].error('Financial report - CAA report error')
    args[:logger].error(error.message.to_s)
  end
end

def testSageExport(args = {})
  begin
    args[:browser].select_list(id: 'whatReport').select('Sage Export')
    args[:browser].select_list(id: 'reportCompany').select('TTNG')
    args[:browser].date_field(id: 'start').set('01/02/2020')
    args[:browser].date_field(id: 'end').set('02/02/2020')
    args[:browser].select_list(name: 'dateTypeSage').select('Create Trigger Date')
    args[:browser].execute_script("document.getElementById('getReport').style.display = 'inline';")
    args[:browser].button(id: 'getReport').click
    sleep(20)
    checkDownloadedFile(logger: args[:logger], fileName: 'sageExport2020-02-01TO2020-02-02.csv',
                        logInfo: 'Sage Export Create Trigger Date')
    args[:browser].select_list(name: 'dateTypeSage').select('Transaction Date')
    args[:browser].button(id: 'getReport').click
    sleep(20)
    checkDownloadedFile(logger: args[:logger], fileName: 'sageExport2020-02-01TO2020-02-02.csv',
                        logInfo: 'Sage Export Transaction Date')
  rescue => error
    args[:logger].error('Financial report - sage export error')
    args[:logger].error(error.message.to_s)
  end
end

def testRemainingAPIs(args = {})
  Net::SSH.start("192.168.227.21", args[:tunnelDetails][0][3].to_s.reverse, password: args[:tunnelDetails][0][4].to_s.reverse) do |ssh|
    updateBooking = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/addDawnChorus?company=ttng&operation=retail&txnDate=2020-07-25&txnNote1=test1'")
    sleep(2)
    if updateBooking == 'OK'
      args[:logger].info('TEST - Add to Dawn Chorus - SUCCESS')
      puts 'TEST - Add to Dawn Chorus - SUCCESS'
    else
      args[:logger].error('TEST - Add to Dawn Chorus - FAILED')
      puts 'TEST - Add to Dawn Chorus - FAILED'
      puts 'Result: ' + updateBooking.to_s
    end

    masterName = ssh.exec!("curl --silent 'http://127.0.0.1:5000/getMasterName?company=ttng&operation=retail&pseudonym=Hertz'")
    sleep(2)
    if masterName == 'Hertz UK Ltd'
      args[:logger].info('TEST - Get Master Name - SUCCESS')
      puts 'TEST - Get Master Name - SUCCESS'
    else
      args[:logger].error('TEST - Get Master Name - FAILED')
      puts 'TEST - Get Master Name - FAILED'
      puts 'Result: ' + masterName.to_s
    end

    lockFreeRobot = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/lockFreeRobot?company=ttng&operation=retail&name=recordISellPayments'")
    sleep(2)
    if lockFreeRobot == '1'
      args[:logger].info('TEST - Lock Free Robot - SUCCESS')
      puts 'TEST - Lock Free Robot - SUCCESS'
    else
      args[:logger].error('TEST - Lock Free Robot - FAILED')
      puts 'TEST - Lock Free Robot - FAILED'
      puts 'Result: ' + lockFreeRobot.to_s
    end

    unlockRobot = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/unlockRobot?company=ttng&operation=retail&name=recordISellPayments&robotSeq=1'")
    sleep(2)
    if unlockRobot == '1'
      args[:logger].info('TEST - Unlock Robot - SUCCESS')
      puts 'TEST - Unlock Robot - SUCCESS'
    else
      args[:logger].error('TEST - Unlock Robot - FAILED')
      puts 'TEST - Unlock Robot - FAILED'
      puts 'Result: ' + unlockRobot.to_s
    end

    setPOpaid = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/setPOpaid?company=ttng&operation=retail&tradeCode=Q0655&id=2298'")
    sleep(2)
    if setPOpaid == '1'
      args[:logger].info('TEST - Set PO Paid - SUCCESS')
      puts 'TEST - Set PO Paid - SUCCESS'
    else
      args[:logger].error('TEST - Set PO Paid - FAILED')
      puts 'TEST - Set PO Paid - FAILED'
      puts 'Result: ' + setPOpaid.to_s
    end

    manUpdatePO = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/manUpdatePO?company=ttng&operation=retail&tradeCode=Q0655&id=2298&bookingreference=NWG-20542'")
    sleep(2)
    if manUpdatePO == '{"result":1}'
      args[:logger].info('TEST - Manual Update PO - SUCCESS')
      puts 'TEST - Manual Update PO - SUCCESS'
    else
      args[:logger].error('TEST - Manual Update PO - FAILED')
      puts 'TEST - Manual Update PO - FAILED'
      puts 'Result: ' + manUpdatePO.to_s
    end

    manUpdatePO = ssh.exec!("curl --silent -X PUT 'http://127.0.0.1:5000/setPOreqLock?company=ttng&operation=retail&tradeCode=Q0655&id=2298&value=y'")
    sleep(2)
    if manUpdatePO == '1'
      args[:logger].info('TEST - Set PO Req Lock - SUCCESS')
      puts 'TEST - Set PO Req Lock - SUCCESS'
    else
      args[:logger].error('TEST - Set PO Req Lock - FAILED')
      puts 'TEST - Set PO Req Lock - FAILED'
      puts 'Result: ' + manUpdatePO.to_s
    end
  end
end